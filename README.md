|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |

# Implementem amb Dockers

Aquesta és una guia pràctica per ajudar-vos a llençar serveis d'una forma ràpida, àgil i consumint pocs recursos. 
Utilitzarem totes les potencialitats que ens ofereix [Docker](https://www.docker.com/resources/what-container). 

Tot seguit veureu un petit manual que podeu seguir i podeu proposar canvis que seran tinguts en compte en la següent versió.



#### Per què Docker?
> És una eina que permet desplegar aplicacions dins de contenedors d'una manera ràpida i portable.

Permet:
- Aplicacions lleugeres pel sistema.
- Desplegar i escalar aplicacions en poc temps.
- Destruir i recrear desplegaments.
- Agilitza moltes tasques de Devops



#### Diferències entre VM & Docker

[imatge]: https://www.wintellect.com/wp-content/uploads/2015/10/docker1.jpg

|Diferències    |Virtual Machine  |Docker         |
| ------------- |:---------------:|:-------------:|
|Rapidesa desplegament|Min's|Segons|
|Tamany|Gb's|Mb's|
|# Processos|Varis processos en VM|1 procés per contenedor|
|Seguretat|Més aïllades|Més propers al host|
|Administració|A càrrec de l'usuari| + Contenedors creats|



#### Docker Hub

Dins de [DockerHub](https://hub.docker.com/) trobareu un repositori amb imatges de sistemes operatius, serveis i moltes configuracions adhoc. 

És recomanable utilitzar les imatges oficials ja que estan optimitzades pel seu ús. Així que us animo a que busqueu aquella que us faci servei, i si no la trobeu, no patiu, la crearem mitjançant [Dockerfiles](../manual/dockerfile.md) 



#### Demo d'ús

Exemples de desplegament d'un apache.

*Instal·lem Docker CE en la nostra màquina (en el meu cas Debian)*<br>[Web instal·lació de Docker per Debian's](https://docs.docker.com/v17.12/install/linux/docker-ce/debian/)

*Em baixo una imatge d'apache de DockerHub [Apache en DockerHub](https://hub.docker.com/_/httpd/)*<br>`$ docker pull httpd `

*Creo el meu procés (Contenedor) a partir d'aquesta imatge*<br>`$ docker run -dit --name contenedorApache -p 8080:80 httpd`

*Uala! En el navegador tinc el servei funcionant*<br>[http://localhost:8080](http://localhost:8080)



#### Què hi trobareu

1. [Instal·lació](../manual/instalacioDocker.md)
2. [Creació d'imatges](../manual/creacioImatges.md)<br>
    2.1. [Dockerfiles](../manual/dockerfile.md)
3. [Creació de Contenidors](../manual/creacioContenedors.md)
4. [Manipulació de volums](../manual/volums.md)
5. [Creació de xarxes](../manual/xarxes.md)
6. [Docker Composer](../manual/composer.md)
7. [Docker Swarm](../manual/swarm.md)



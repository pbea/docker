|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 6. Docker Compose

> Docker Compose ens permet definir en un fitxer la configuració de diferents contenedors de forma conjunta, definint les xarxes, els volums, les imatges i els contenedors que hi haurà en la nostra aplicació.
> És una eina que ens ajuda a crear aplicacions multicontenedor. Per exemple un contenedor podria ser un webserver, un altre una base de dades i podrien estar connectats per una xarxa que nosaltres definim i també mitjançant un volum es guardaria la informació de l'aplicació.

#### Instal·lació

En [aquesta pàgina](https://docs.docker.com/compose/install/#install-compose) us indica com s'instal·la per a cada sistema operatiu.

En Debian ens descarreguem el paquet:<br>
`curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose`

Donem permisos d'execució a aquest fitxer i l'executem:<br>
`chmod +x /usr/local/bin/docker-compose`<br>
`/usr/local/bin/docker-compose`

Si cliqueu ara `docker-compose` us apareixeran el conjunt d'opcions que té aquesta comanda.


#### Configuració

Per tal de configurar i executar un fitxer compose, crearem una carpeta i dins un fitxers docker-compose.yml. 

Dins aquest fitxer pot contenir 4 parts importants:
- version 
- services
- volumes
- networks

Com s'observa [aquí](https://docs.docker.com/compose/compose-file/#service-configuration-reference) actualment la versió més nova és la 3, així que hi posarem un 3. Dins de services contindrà tots els contenedors que volguem utilitzar. Per a cadascun li indicarem un nom, i dins una sèrie de [paràmetres](https://docs.docker.com/compose/compose-file/#container_name). Tot seguit mostro un exemple d'un sol contenedor que obre un apache i com seria el corresponen docker-compose:<br>
`docker run -d -p 8080:80 --name apache httpd`<br>

El corresponent fitxer docker-compose.yml:
~~~
version: '3'
services:
    webserver:
        container_name: apache
        ports:
            - "8080:80"
        image: httpd
~~~

Per tal de llançar aquest docker-compose:<br>
`docker-compose up -d`

Si el nom del fitxer fos diferent a docker-compose.yml, usaríem el flag -f nomFitxer.yml.


Crearà una xarxa i llançarà a dins el/s servei/s que hi hagi definits en el fitxer.

Per tal de parar l'aplicatiu utilitzarem:<br>
`docker-compose down`

##### Variables d'entorn

Per incloure variables d'entorn farem servir la paraula environment. Com sempre podeu visitar la [documentació](https://docs.docker.com/compose/compose-file/#environment) per ampliar-ho. Ex:
~~~
version: '3'
services:
    bbdds:
        container_name: mysql
        ports:
            - "3306:3306"
        image: mysql:5.7
        environment:
            - "MYSQL_ROOT_PASSWORD=1234"
~~~

Una altra manera de definir les variables d'entorn d'una forma ben estructurada és usant un fitxer variables.env on hi hagi les variables, llavors en el fitxer .yml enlloc de environment usaríem env_file:
~~~
version: '3'
services:
    bbdds:
        container_name: mysql
        ports:
            - "3306:3306"
        image: mysql:5.7
        env_file: variables.env
~~~

i dins aquest fitxer, i pel cas particular d'un mysql, podria tenir:
~~~
MYSQL_ROOT_PASSWORD=1234
MYSQL_DATABASE=ddbb
MYSQL_USER=docker
MYSQL_PASSWORD=1234
~~~

##### Volums

Per tal de definir volums (volum nombrat) i linkar el nostre contenedor amb l'equip local ho farem de la següent manera, mitjançant un exemple. Sense docker compose faríem:<br>
`docker volume create volum`<br>
`docker run -d -p 8080:80 --name nginx -v "volum:/usr/share/nginx/html" nginx`

Si volem realitzar el mateix usant docker compose, s'ha de definir el volum i linkar-lo en el servei corresponent:
~~~
version: '3'
services:
    webserver:
        container_name: nginx
        ports:
            - "8080:80"
        volumes:
            - "volum:/usr/share/nginx/html"
        image: nginx
volumes:
    volum
~~~

**És important mantenir l'identat per a que el fitxer .yml entengui quines jerarquies hi ha.**

Si el que volem és realitzar un volum de host faríem:
~~~
version: '3'
services:
    webserver:
        container_name: nginx
        ports:
            - "8080:80"
        volumes:
            - "directoriFitxersLocal:/usr/share/nginx/html"
        image: nginx
~~~

On 'directori fitxers local' seria on tenim ubicat els fitxers en el nostre pc.

##### Xarxes

Per incloure les xarxes en el docker-compose és tant senzill com definir la xarxa al final i incloure-les en cadascun dels serveis. Aquí teniu un exemple de dos serveis corrent a la mateixa xarxa:
~~~
version: '3'
services:
    webserver:
        container_name: apache1
        ports:
            - "8080:80"
        image: httpd
        networks:
            - xarxa    
    webserver2:
        container_name: apache2
        ports:
            - "8080:80"
        image: httpd
        networks:
            - xarxa    
networks:
    xarxa:
~~~


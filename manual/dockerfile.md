|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 2.1. Dockerfile

#### Per a què serveix un Dockerfile?

> Són fitxers que ens permeten crear les nostres pròpies imatges.

Un Dockerfile és un fitxer que s'anomena tal qual 'Dockerfile' en el que s'espefica QUÈ ha de contenir la meva imatge, per tal de crear una imatge a partir del Dockerfile en el CLI posarem:<br>
`docker build --tag nomImatge .`

Mitjançant --tag donarem nom a la imatge que estem construint i el . simbolitza el Dockerfile del directori on ens trobem.

Observareu que mentre va construint la imatge va passant per diferents steps que no són més que les capes que s'ha indicat en el Dockerfile. Quan en una capa troba un recurs que ja té al propi local agafa aquest, sinó, automàticament farà un pull request i buscarà aquell recurs que necessita en DockerHub.

#### Estructura del Dockerfile

Un Dockerfile s'estructura a partir de les capes que hi posem, així doncs normalment contindrà una capa FROM, una o més capes RUN i una cap CMD. Anem a identificar tipus diferents de capes que puc tenir:

Tipus | Explicació
 ---- | ----------
 FROM | Imatge base a partir de la qual es parteix per construir la nova imatge.
 RUN  | Ens permet executar comandes en el /bin/bash per defecte o també instruccions exec. [+ info](https://docs.docker.com/engine/reference/builder/#run)
 COPY/ADD | Ens permet copiar/afegir fitxers dins l'arxiu de directori de la nova imatge.
 ENV | Ens permet crear variables d'entorn, molt útil per configurar base de dades on calen paràmetres d'entrada per exemple.
 WORKDIR | Ens defineix el directori per defecte.
 EXPOSE | Es mostra el port pel qual escoltarà el servei.
 LABEL | Permet posar etiquetes informatives.
 USER | Permet canviar d'usuari en una configuració.
 VOLUME | Ens indica on guardarem informació persistent sobre aquest Contenedor que creem.
 CMD  | Permet identificar el propòsit principal pel qual es crea el container.

Important remarcar el **.dockerignore**. Les imatges poden contenir directoris propis amb informació pròpia. A vegades existeix informació que no aporta res a la imatge i que l'únic que fa és augmentar-ne el tamany i per tant per una de les seves potencialitats. Per això, en .dockerignore escriurem línia a línia aquells arxius que no cal que formin part de la nostra imatge i que per tant en reduiran el tamany.

En el següent link trobareu exemples de [Dockerfiles](../Exemples%20Dockerfile) per diferents propòsits.


#### Canviar el nom a un Dockerfile
Si el fitxer Dockerfile tingués un altre nom executaríem la següent instrucció en el CLI:
`docker build --tag nomImatge --f nomDockerfile .`



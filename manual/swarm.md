|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 7. Docker Swarm

> Docker Swarm és una eina que ens proporciona Docker que ens permet connectar i treballar plegats diferents nodes amb diferents contenedors connectats.

**Característiques**
 - Proporciona un disseny amb nodes **distribuïts**, on cadascun conté els seus contenedors.
 - Descobriment de serveis: per cada servei s'utilitza un nom DNS únic controlat pel propi swarm.
 - Balanceig de càrrega: es pot deixar els ports dels diferents serveis per a un balancejador de càrrega extern, permet distribuir la càrrega dels serveis entre els nodes.
 - Seguretat: utilitza TLS per l' autentificació i encriptació de la comunicació entre nodes.
 - Monitorització constant de l'estat del cluster i assegurament del servei.
 - Escalat: permet controlar l'augment o reducció de tasques/instàncies per a cada servei.

**Configuració**

Diferenciem per dos rols diferents:
- Manager: gestió i administració.
- Worker: execució dels serveis.
    
Un servidor Docker pot tenir nodes manager i worker corrent al mateix temps.

Quan creem un servei n'exposem l'estat òptim:
- Nombre de rèpliques.
- Xarxa
- Emmagatzemament
- Ports

Swarm s'encarrega de gestionar l' estat òptim en l'execució dels contenedors.

Els ports que utilitza Swarm són:

- tcp 2377 per comunicacions al Manager
- tcp/udp 7946 comunicacions entre nodes
- udp 4789 pel tràfic de la nostra xarxa docker. (“overlay”)


##### Cas pràctic 1
En el següent link a l'apartat "Laboratorio", trobareu un bon exemple de desplegament senzill de Docker Swarm. Consisteix en 3 hosts. El primer es crea el docker swarm i s'inicialitza com a master. La resta s'uneixen (join) amb aquest master. A partir d'aquí el master és qui defineix quantes rèpliques fa d'un servei i s'encarrega de distribuir-les entre ell i els workers. Es poden fer proves d'escalabilitat modificant el paràmetre --scale i es veurà com el manager s'encarrega de distribuir el llançament de contenedors.

[Bon exemple](https://robertoorayen.eu/2018/03/18/docker-swarm/)

##### Cas pràctic 2

Aquest és un exemple **molt** interessant. 
- En aquest cas es crea 3 managers i 3 workers. 
- Tot seguit s'assigna en el 'manager leader' un contenedor nginx. 
- Veurem com en la ip d'aquest i de qualsevol altre node existeix un enrutament, això és gràcies al [Routing mesh](https://docs.docker.com/engine/swarm/ingress/), qualsevol node connectat al swarm pot respondre peticions de qualsevol node. 
- A continuació apliquem l'escalabilitat i es creen 15 serveis nginx i es veu com es reparteixen entre managers i workers. 
- Després fem que caigui un dels workers i veiem com es reparteixen els serveis entre els nodes restants.
- Podem reduir l'escalabilitat dels 15 serveis nginx al que volguem, en l'exemple 10 i veiem com alguns contenedors es posen en "shutdown".
- També podem fer que el 'manager leader' surti del swarm, de forma que automàticament s'assigna a un dels managers la condició de leader.
- Finalment es dóna de baixa tots els nodes.

[Exemple amb 6 nodes i 15 serveis nginx](https://github.com/docker/labs/blob/master/swarm-mode/beginner-tutorial/README.md)


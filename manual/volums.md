|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 4. Manipulació de volums

**IMPORTANT**
> Les imatges són de només **Lectura**, mentre que els contenedors són de **Lectura/Escriptura.**

> Un cop es deixen d'executar els contenedors la memòria que utilitzaven **desapareix** ja que és **volàtil.**

> Els **volums** ens permeten guardar les dades de **forma persistent** dels contenedors. 

> D'aquesta manera podem **crear i destruir** el mateix contenedor tantes vegades com volguem que les **dades** es mantindran **permanents.**

#### Tipus de volums

##### 1. Host
En aquest cas guardo la informació en una carpeta que indiquem explícitament quina és. 

Passos:
1. Creo una carpeta, per exemple /opt/mysql
2. Faig corre el contenedor de la següent manera:<br>
`docker run -d --name bbdds_mysql -p 3306:3306 -e "MYSQL_PASSWORD=1234" -v /opt/mysql:/var/lib/mysql`

Important, fixeu-vos que -v ens indica que utilitzem un volum. Mapegem la nostra carpeta /opt/mysql on dins el contenedor tindrem guardades les nostres dades, en el cas d'un servei mysql serà /var/lib/mysql. Aquesta informació es pot treure de DockerHub, si observeu la documentació us diu on es guarda dins de cada servei la informació de forma persistent.

Si volguéssim tenir més d'una instància mysql mapejaríem el nostre port (el primer) en un de diferent al 3306 (ja que ja estaria ocupat).

Podeu provar de crear taules i dades dins de la base de dades: Elimineu el contenedor, torneu-lo a crear i veureu com les dades són persistents i es guarden en la carpeta local que li hem indicat. (En borrar no useu el flag -v,  ja que borraríeu també el volum  `docker rm -f nomContenedor`)

##### 2. Anònims
Els volums anònims guarden la informació dins la carpeta que té docker de dades. El problema que tenim és que una vegada eliminat el contenidor, com que l'arxiu de dades es crea mitjançant un identificador en podem perdre la referència. Anem a crear-ne un.

Primer de tot cal saber a on tenim la carpeta de dades on docker guardarà la informació persistent dels contenedors. Per saber on tenim ubicat docker:<br>
`docker info | grep -i root`

Aquesta direcció que retorna és on dins trobarem una carpeta "volumes" que és on es guarden les dades persistents (si no indiquem destí).

Anem a crear un contenedor:<br>
`docker run -d --name bbdds_mysql -p 3306:3306 -e "MYSQL_PASSWORD=1234" -v /var/lib/mysql`

Fixeu-vos que en aquest cas no indico a on guardo en local les dades, ja que es guardaran "per defecte" a la carpetaDocker/volumes.

Si entrem en aquesta carpeta veurem un arxiu nou amb un identificador random creat. Dins d'aquest arxiu trobarme una carpeta '_data' i allí és on tenim les dades guardades.

Per comprovar que és aquest volum el que s'ha creat, podem veure en la següent comanda quina informació pertany a aquest contenedor, entre ells veureu l'identificador d'aquest volum:<br>
`docker inspect nomContenedor`

**El problema** el tenim quan eliminem aquest contenedor, com que es perd la referència, les dades estan guardades dins de direccioDocker/volumes, però si torno a crear el contenedor docker no té manera d'identificar la base de dades **anònima** que hem creat en la instància anterior.

**La solució** transformarem el volum en Host i deixarà de ser anònim:<br>
`docker run -d --name bbdds_mysql -p 3306:3306 -e "MYSQL_PASSWORD=1234" -v direccioDocker/volumes/idVolum/_data:/var/lib/mysql`

D'aquesta manera Docker reconeix localment on es guarda la informació corresponent a aquesta base de dades.


##### 3. Named Volumes

En aquest cas parlem d'una barreja entre les host i les anònimes. Ens permet definir un volum donant-li un nom, aquest volum es trobarà ubicat en la carpeta "direccioDocker/volumes/".

Per tal de **crear** el volum:<br>
`docker volume create nomVolum`

I alhora d'associar-lo amb un contenedor en concret:<br>
`docker run -d -v nomVolum:direccioServeiContenedor nomContenedor`<br>

Per exemple si tingués un volum 'myVolume' on vull guardar una base de dades mysql:<br>
`docker run -d -p 3306:3306 -v myVolume:/var/lib/mysql --name mysql -e "MYSQL_ROOT_PASSWORD=1234" -e "MYSQL_DATABASE=myDatabase" mysql:5.7`

En aquest cas, respecte als volums anònims, si borro el contenedor no es borrarà el volum. Així doncs:
`docker rm -fv mysql` no borraria el volum ja que és named volume, si fos anònim deixaria d'estar-hi referenciat.


###### Dangling Volumes

Podria passar que fent proves tinguem molts volums que no estan referenciats i que l'únic que fan és ocupar memòria (igual com ens passava amb la creació de les imatges), el que podríem fer és eliminar-los:<br>
`docker volume ls -f dangling=true -q | xargs docker volume rm`<br>

Llistarà tots els ids dels volums que no estan referenciats a cap contenedor i els eliminarà un a un.

#### Exemples

Per a cada cas he buscat en Docker Hub cadascun del servei per saber en quina carpeta es guarda les dades, aquesta carpeta és la que he mapejat en un volum local.

**Mongo**
Guardem informació d'una base de dades:<br>
`docker run -d -p 27017:27017 -v /opt/mongo/:/data/db_mongo mongo`

**Jenkins**
Guardem informació sobre un aplicatiu com jenkins:<br>
`docker run -d -p 8080:8080 -v /home/pep/jenkins/:/var/jenkins_home jenkins`

**Nginx**
Guardem la informació d'un determinat log de nginx:<br>
`docker run -d -p 80:80 -v /home/pep/nginx:/var/log/nginx/ nginx`




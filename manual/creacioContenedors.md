|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 3. Creació de Contenedors
> Aspectes clau
> - El Contenedor és una instància en execució d'una imatge de Docker.
> - El nostre ordinador veu cada contenedor com un procés més a executar.
> - Són volàtils / temporals, això vol dir que una vegada s'acabi el procés en execució d'aquest container, si no tenim un volume associat, totes les dades es perden.
> - Les capes de les imatges són de només L, mentre que les capes dels contenedors són de L/E, això vol dir que les podem sobreescriure (instal·lar paquets, introduir dades, etc), però recordeu que un cop s'acabi aquest procés, totes les modicacions que hi heu fet, tant de software com de dades es perden.
> - Podem tenir tants contenedors de la mateixa imatge com volguem, sempre que respecti els límits de memòria i cpu que suporta el nostre ordinador. 


Per tal d'executar un contenedor usarem la següent instrucció (en aquest cas el mapeig de ports correspon a un servidor web):<br>
`docker run -d -p 8080:80 -t nomContenedor nomImatge` 


Amb l'opció `-e "nomVariable=valor"` ens permet passar-li una variable d'entorn que podem usar dins el contenedor, per exemple una contrassenya de bbdd's, nom d'una bbdd's o nom d'un domini.


Errors comuns:
- Ports: **No mapejar** els ports del contendor a la nostra màquina, i per tant el contenedor no sap a quin servei es refereix.
- Crear diferents contendors amb la mateixa imatge i al mateix port (només un estarà actiu, el primer, s'ha de canviar el port sobre on es vol actuar en local, per a que cada servei escolti a un port diferent). 

Per més informació [docker run](https://docs.docker.com/engine/reference/commandline/run/#options).


Per tal de **veure** quins contenedors tenim en **execució** usarem:<br>
`docker ps`


Si volem **veure** tots els contenedors que tenim, **en execució i parats** (per diferents causes):<br>
`docker ps -a`


Per **renombrar** el meu contenedor:<br>
`docker rename nomActual nomNou`


**Iniciar/reiniciar/parar** el meu contenedor:<br>
`docker start idContenedor/nomContenedor`<br>
`docker restart idContenedor/nomContenedor`<br>
`docker stop idContenedor/nomContenedor`


Per **entrar** en un contenedor:<br>
`docker exec -ti nomContenedor bash`


Per **copiar** arxius de fora a dins del contenedor i viceversa usarem (docker cp):<br>
Per copiar cap a dins del contenedor: `docker cp rutaArxiu nomContenedor:/rutaDesti`.
Per copiar en local un document del contenedor: `docker cp nomContenedor:/rutaArxiu rutaLocalDesti`.


Podeu comprovar que existeix entrant dins el contenedor (docker exec) i comprovareu que el/s fitxer/s s'han copiat.


Podreu observar com ara mateix estem dins el contenedor en un terminal bash on podem efectuar-hi qualsevol consulta que ens deixi. (exit per sortir)


Per **eliminar** un contenedor usarem:<br>
`docker rm -vf nomDelContenedor`
`docker rm -vf identificador`


Per tal **d'eliminar** totes les instàncies:<br>
`docker rm $(docker ps -aq)`


Per tal de visualitzar **què està passant** en el contenedor:<br>
`docker logs -f nomContenedor`


Per tal d'observar la quantitat de recursos que empra un contenedor (cpu i memòria ram):<br>
`docker stats nomContenedor`

Per a fer-ho en la comanda run cal posar un -m "200mb" per exemple, o bé --cpuset-cpus 0-1 per definir que només usi els cores 1 i 2.


## Exemples

#### Mysql

Cal definir [MYSQL_ROOT_PASSWORD](https://hub.docker.com/r/library/mysql/):<br>
`docker run --name mysql -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=12, MYSQL\_ROOT\_PASSWORD34" -d mysql:tag`


També són útils les variables d'entorn MYSQL\_USER, MYSQL\_ROOT\_PASSWORD i MYSQL\_DATABASE.


#### Mongo

Seria de la següent[manera](https://hub.docker.com/r/library/mongo/):<br>
`docker run --name mongo -p 27017:27017 -d mongo:tag`


Recordeu en cas d'utilitzar dos serveis de base de dades igual quan es mapeja el segon hauria d'estar en un port diferent _-p 27018:27017_ en la nostra màquina.


#### Postgresql

Per tal de buscar cada paràmetre podeu mirar la pàgina de DockerHub de [postgresql](https://hub.docker.com/_/postgres/):<br>
`docker run --name postgres -p 5432:5432 -d -e "POSTGRES_PASSWORD=1234" -e "POSTGRES_USER=docker" -e "POSTGRES_DB=dockerDB" postgres`


#### Nginx / Apache / Tomcat

Per tal de crear contenedors amb servidors web (fixeu-vos que canvio el port per a que no coincideixin):<br>
`docker run -d -p 8888:80 --name nginx nginx`<br>
`docker run -d -p 9999:80 --name apache httpd`<br>
`docker run -d -p 7070:8080 --name tomcat tomcat`

Podeu provar-ho accedint als localhost:8888 localhost:9999 localhost:7070, també mitjançant 'docker ps' veureu els tres contenedors en execució. Observeu que si no troba la imatge en local, automàticament docker la busca i fa un pull de DockerHub.


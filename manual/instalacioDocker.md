|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 1. Instal·lació Docker

Primer de tot cal tenir instal·lat Docker al nostre ordinador. [Aquí teniu com instal·lar Docker en diferents sistemes operatius.](https://docs.docker.com/install/)

Mostro a tall d'exemple com ho faríem en Debian i Ubuntu, molt semblant, canviarem només els repositoris.


##### Debian

*Actualitza les repos*<br>
`sudo apt-get update`
  
*Instal·la utilitats*<br>
`sudo apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common -y`
  
*Agrega gpg*<br>
`curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -`
  
*Agrega el repo*<br>
`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"`

*Actualitza de nou*<br>
`sudo apt-get update`

*Instal·lar docker*<br>
`sudo apt-get install docker-ce`

*Iniciar-lo amb el sistema*<br>
`sudo systemctl enable docker`

*Agregar usuari al grup docker*<br> 
`whoami`<br>
`sudo usermod -aG docker nom_sortida_whoami`

*Iniciar de nou amb l'usuari i hello-world*<br> 
`docker run hello-world`


##### Ubuntu

*Actualitza les repos*<br>
`sudo apt-get update`
  
*Instal·la utilitats*<br>
`sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y`
  
*Agrega gpg*<br>
`curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
  
*Agrega el repo*<br>
`sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`

*Actualitza de nou*<br>
`sudo apt-get update`

*Instal·lar docker*<br>
`sudo apt-get install docker-ce`

*Iniciar-lo amb el sistema*<br>
`sudo systemctl enable docker`

*Agregar usuari al grup docker* <br>
`whoami`
`sudo usermod -aG docker nom_sortida_whoami`

*Iniciar de nou amb l'usuari i hello-world*<br> 
`docker run hello-world`


[Com instal·lar Docker a Windows](https://docs.docker.com/docker-for-windows/install/)


[Com instal·lar Docker a MAC](https://docs.docker.com/docker-for-mac/)

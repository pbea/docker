|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 2. Creació d'Imatges

#### Què és una imatge?

> Una imatge és una configuració d'un servei i/o un sistema operatiu preparat per ser executat. 

Les imatges de Docker s'estructuren en base a capes. Es recomana fer imatges amb poques capes, ja que interessa que siguin lleugeres. 
Inicialment existeix una capa de SO molt lleugera que sustenta el/s servei/s de les capes posteriors. 
Quan descarregueu una imatge observeu com es van descarregant totes les capes que té associades, si alguna d'aquelles capes ja està baixada en el teu equip veureu com l'agafa 'cachejada' ("Already Exists").


#### Què conté Docker Hub? 
> [DockerHub](https://hub.docker.com/) és un repositori de Dockers oficials creats per la comunitat on pots descarregar-te aquella imatge que necessitis.

En cada imatge existeixen dues pestanyes: Repo Info i Tags. 
Dins de Repo Info us mostra informació sobre què conté la imatge i us dóna indicacions específiques de configuració i de desplegament per a cada cas.
Dins de Tags, us informa de les diferents versions d'aquesta imatge, actualitzacions, vulnerabilitats, etc.

#### Com administrem imatges? 
A la columna dreta (per exemple per una imatge de mongo), veureu la següent comanda que us 'baixarà' la imatge en local:<br>
`docker pull mongo`

Per cada imatge veureu que té un conjunt de tags que identifiquen cada versió. Si vull fer un pull d'una versió en concret:<br>
`docker pull mongo:3.2-jessie`.

El tag **latest** fa referència a la última versió no taggejada.

Sempre que vulgui **visualitzar** quines imatges tinc en local:<br>
`docker images`

Si tinc moltes imatges i vull veure'n una  amb un servei en concret: <br>
`docker images | grep nomServei`

Per **eliminar** una imatge puc usar com a identificador de la imatge: 'nom:tag' o bé l'identificador que genera per a cada imatge docker:<br>
`docker rmi nom`<br>
`docker rmi nom:tag`<br>
`docker rmi id_generat`


Què són les **'dangling images'**? 
Com observareu si creeu més d'una imatge amb el mateix servei, la referència serà sempre la última imatge creada amb aquest nom, però en canvi les imatges no es borren, les diferents versions creades anteriorment estan guardades i identificades com a <none> la qual cosa ocupen un espai innecessari, si les volem esborrar:<br>
`docker images -f dangling=true -q | xargs docker rmi`

Aquesta consulta el que fa és amb -f dangling=true sel·lecciona els dockers que són dangling, d'aquests amb -q ens quedem l'id. Tot seguit aquests id's es passen a docker rmi per esborrar-los un a un.

Sempre em puc ajudar en qualsevol comanda de **--help** per saber quins paràmetres puc utilitzar. Per exemple:<br>
`docker images --help`



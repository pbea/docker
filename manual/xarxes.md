|   [Instal·lació](../manual/instalacioDocker.md)   |   [Imatges](../manual/creacioImatges.md)   |   [Dockerfiles](../manual/dockerfile.md)   |   [Contenedors](../manual/creacioContenedors.md)   |   [Volums](../manual/volums.md)   |   [Xarxes](../manual/xarxes.md)   |   [Compose](../manual/compose.md)   |   [Swarm](../manual/swarm.md) |


# 5. Creació de xarxes

Tot seguit mostrarem quins tipus de xarxes existeixen, com crear i configurar una xarxa utilitzant diferents contenedors de Docker i com connectar-nos a una xarxa.

##### Tipus

- Bridge: és la xarxa on es situen per defecte tots els contenedors.
- Host: els contenedors amb l'opció host tindran la mateixa configuració de xarxa que el propi host.
- None: és un tipus de xarxa que no assigna ip al contenedor i per tant el tenim aïllat de la resta de contenedors.



### Configuració inicial

Per defecte podem veure en quina xarxa es troben les màquines docker amb la comanda:<br>
`ip a | grep docker`

Aquesta xarxa és la que usarà per defecte i com a bridge per a totes les màquines si no se li indica res diferent.

Mitjançant `docker inspect nomContenedor` veurem en quina xarxa es troba situada en Networks->bridge, també ens indica el gateway i el prefix de xarxa.

Mitjançant `docker network ls` mostra les xarxes existents.

Si volem informació sobre una xarxa determinada `docker network inspect nomXarxa`.

Per tal de fer una prova i observar que tots els contenedors es creen dins la mateixa xarxa per defecte, podeu entrar usant 'docker exec' en un contenedor i mirar de fer ping cap a altres contenedors.


### Crear i connectar contenedors

Per tal de **crear** una xarxa podeu usar:<br>
`docker network create --subnet 172.10.1.0/32 --gateway 172.10.1.1 nomXarxa`

Tant el subnet com el gateway són opcionals, s'ha de mirar de donar una xarxa privada i que no es superposi amb cap de les ja existents.

Per tal de **connectar** un contenedor dins una xarxa només cal afegir el flag '--network nomXarxa' dins de la comanda run. S'assignarà una ip vàlida dins el rang del nomXarxa. Dins els contenedors existeix una peculiaritat, i és que podem fer ping entre contenedors situats dins la mateixa xarxa usant el nom del contenedor (Ex. ping nginx, també es pot fer amb la ip assignada)

Una bona pràctica podria consistir en crear una xarxa i assignar-li dos contenedors, tot seguit entrar dins d'un d'ells 'docker exec' i fer un ping nomContenedor cap a l'altre contenedor.

**Connectar un contenedor a una xarxa diferent a la seva**: Per tal de poder obtenir connectivitat entre contenedors situats en xarxes diferents, Docker el que fa és afegir un dels dos contenedors en la xarxa que no és la seva, de forma que internament crea una interfície amb una ip amb la xarxa nova adquirida:<br>
`docker network connect nomXarxa nomContenedor`

Per tal de comprovar-ho el que podeu fer és un `docker inspect nomContenedor` i veureu com ara en Networks apareix la xarxa on ja estava ubicat aquest contenedor, més una nova xarxa.

**Eliminar xarxa**: per tal d'eliminar una xarxa usarem `docker network rm nomXarxa`, ens ha de quedar clar que no es pot eliminar una xarxa si hi té contenedors associats, així que cal abans borrar els contenedors associats a aquesta xarxa. 

**Associar un contenedor a una ip en particular**: ens podria interessar connectar un contenedor a una ip en particular, es realitza dins la comanda run amb el flag '--ip ipNumerica/mascara'. Exemple:<br>
`docker run -d --network nomXarxa --ip 172.10.10.2/24 --name nginx1 -ti centos`

### Host

Per tal de connectar el nostre ordinador a host només cal que en el flag '--network host', això el que farà serà donar la mateixa configuració de xarxa que té la màquina local al contenedor.

### None

Si volem que un contenedor estigui aïllat i no volem que tingui configuració de xarxa només cal posar al flag '--network none', veurem amb la comanda `docker inspect nomContenedor` que el contenedor no té assignada cap configuració de xarxa.



